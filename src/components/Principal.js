import { useEffect, useState } from "react";
import Button from "react-bootstrap/esm/Button";
import { db } from './firebase';
import Producto from "./productos/Producto";
import { Link } from "react-router-dom";
import "./Principal.css"

function Principal(props) {

  const [productos, setProductos] = useState([]);
  const [pedido, setPedido] = useState([]);
  const [pedidoTotal, setTotalPedido] = useState([]);
  let totalPedido = 0;
  let arrayProductos = [];

  useEffect(() => {
    db.collection('Productos')
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          var elemento = doc.data();
          arrayProductos.push({
            id: elemento.id,
            nombre: elemento.nombre,
            precio: elemento.precio,
            fecha: elemento.fecha,
            rutaImagen: elemento.rutaImagen
          })
        });
        setProductos(arrayProductos);
      }).catch(error => {
        console.log('Se ha producido un error ' + error);
      });
  }, []);

  let obtenerPedido = function () {
    let arrayPedido = [];
    setTotalPedido(0)

    db.collection('Usuarios').doc(props.loginData.email).collection("Cesta")
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(doc => {
          var elemento = doc.data();
          totalPedido += elemento.precio * elemento.cantidad;
          arrayPedido.push({
            id: elemento.id,
            nombre: elemento.nombre,
            precio: elemento.precio,
            cantidad: elemento.cantidad
          })
        });
        setTotalPedido(totalPedido)
        setPedido(arrayPedido);
      }).catch(error => {
        console.log('Se ha producido un error ' + error);
      });
  }

  return (
    <div>
      <div className="container-fluid">
        <div className="row">
          <div className="col-7" style={{ margin: '5px' }}>
            <div className="row">
              {productos.map((elemento) => (
                <Producto
                  obtenerPedido={obtenerPedido}
                  id={elemento.id}
                  nombre={elemento.nombre}
                  precio={elemento.precio}
                  fecha={elemento.fecha}
                  rutaImagen={elemento.rutaImagen}
                  idToken={props.loginData}
                />
              ))}
            </div>
          </div>
          <div className="col-4" id="tramitar_cesta">
            <div className="card bg-dark " >
              <div className="card-header">
                <Button style={{ textDecoration: 'none' }} id="btn_tramitar" className="btn-dark text-white"><Link to="/tramitar" style={{ textDecoration: 'none' }} className="text-white">Tramitar pedido</Link></Button>
              </div>
              <ul className="list-group list-group-flush">
                {pedido.map((elemento) => (
                  <li className="list-group-item">
                    {elemento.nombre} x {elemento.cantidad} = {elemento.cantidad * elemento.precio}
                  </li>
                ))}
                <li className="list-group-item">
                  {console.log(pedidoTotal)}
                  Total: {Math.round(100 * pedidoTotal) / 100}
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Principal;