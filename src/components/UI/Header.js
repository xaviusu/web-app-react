import { Link } from "react-router-dom";
import "./Header.css"

function Header(props) {
    return (
        <nav className="navbar navbar-expand navbar-dark bg-dark">
            <div className="container-fluid">
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link style={{ textDecoration: 'none' }} className="nav-link active" to="/">Inicio</Link>
                    </li>
                    <li className="nav-item">
                        <Link style={{ textDecoration: 'none' }} className="nav-link active" to="/pedidos">Pedidos</Link>
                    </li>
                </ul>
                <form className="d-flex" id="form_pedidos">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link style={{ textDecoration: 'none' }} className="nav-link active" to="/login">Login</Link>
                        </li>
                        <li className="nav-item">
                            <Link style={{ textDecoration: 'none' }} className="nav-link active" to="/registro">Registro</Link>
                        </li>
                    </ul>
                </form>
            </div>
        </nav>
    );
}
export default Header;

