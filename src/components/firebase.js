
import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyCoVgwF5b_XBZdAvv7aDCEXnRVzE84lBSM",
    authDomain: "web-app-react-3bbfa.firebaseapp.com",
    projectId: "web-app-react-3bbfa",
    storageBucket: "web-app-react-3bbfa.appspot.com",
    messagingSenderId: "1086843958856",
    appId: "1:1086843958856:web:51a895c888c723ba858a7d",
    measurementId: "G-XF564NGMQE"
  };

  firebase.initializeApp(firebaseConfig);
export const db = firebase.firestore();
