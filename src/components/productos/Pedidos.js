import { useEffect, useState } from "react";
import { db } from "../firebase";
import "./Pedidos.css"

function Pedidos(props) {

    const [pedidos, setPedidos] = useState([]);
    const [detallesPedidos, setDetallesPedidos] = useState([]);

    const borrarPedido = function (id) {
        db.collection("Usuarios").doc(props.loginData.email).collection("Pedidos").doc(id).delete();
        obtenerPedidos();
    }

    useEffect(() => {
        obtenerPedidos();
    }, []);

    const obtenerPedidos = () => {
        db.collection('Usuarios').doc(props.loginData.email).collection("Pedidos").get().then(querySnapshot => {
            let arrayPedidos = [];
            let arrayTotalPrecio = [];
            let i = 0;
            let precioPedido;
            querySnapshot.forEach(doc => {
                arrayPedidos.push({
                    id: doc.id,
                    nombre: "pedido" + i,
                    fecha: doc.data().fechaPedido,
                    hora: doc.data().horaPedido
                })
                i++;
            });
            setPedidos(arrayPedidos);
            for (let j = 0; j < arrayPedidos.length; j++) {
                db.collection('Usuarios').doc(props.loginData.email).collection("Pedidos").doc(arrayPedidos[j].id).collection("Detalle pedido").get().then(querySnapshot2 => {
                    precioPedido = 0;
                    let arrayDetallesPedido = [];
                    querySnapshot2.forEach(doc2 => {
                        var elemento2 = doc2.data();
                        arrayDetallesPedido.push({
                            id: arrayPedidos[j].id,
                            nombre: elemento2.nombre,
                            cantidad: elemento2.cantidad,
                            precio: elemento2.precio
                        })
                        precioPedido += parseFloat(elemento2.precio);
                    })
                    setDetallesPedidos(arrayDetallesPedido);
                }).catch((error) => {
                    console.log(error);
                });;
            }
        }).catch((error) => {
            console.log(error);
        });

    }

    if (props.loginData.email !== undefined) {
        return (
            <div>
                <div className="pedidos_container">
                    <div className="accordion" id="accordionExample">
                        {pedidos.map((elemento) => (
                            <div>
                                <button className="btn-primary" style={{ margin: 'auto' }} onClick={() => borrarPedido(elemento.id)}>Eliminar pedido</button>
                                <div className="accordion-item">
                                    <h2 className="accordion-header" id="headingOne">
                                        <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target={"#" + elemento.nombre} aria-expanded="true" aria-controls="collapseOne">
                                            {elemento.fecha} {elemento.hora}
                                        </button>
                                    </h2>
                                    <div id={elemento.nombre} className="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                        <div className="accordion-body">
                                            <div>
                                                <ul className="list-group list-group-flush">
                                                    {detallesPedidos.map((elemento) => (
                                                        <li className="list-group-item">
                                                            <div>{elemento.nombre} x {elemento.cantidad} = {elemento.precio}</div>
                                                        </li>
                                                    ))}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        );
    } else {
        return (
            <div>Tienes que estar logueado.</div>
        )
    }
}
export default Pedidos;