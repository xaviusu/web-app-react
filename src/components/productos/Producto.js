import Button from 'react-bootstrap/esm/Button';
import { db } from '../firebase';
import './Producto.css';
import { useState } from 'react';
import React from 'react';

function Producto(props) {
    const id = props.id;
    const nombre = props.nombre;
    const precio = props.precio;
    const rutaImagen = props.rutaImagen;
    let cantidad = 0;
    const [buttonClicked, setButtonClicked] = useState(0);
    const actualizarPedido = props.obtenerPedido;

    const anadirProductoCesta = () => {
        if (props.idToken.idToken != undefined) {
            cantidad = buttonClicked + 1;
            setButtonClicked(cantidad);
            console.log(props.idToken);
            db.collection("Usuarios").doc(props.idToken.email).set({
                email: props.idToken.email
            });
            db.collection("Usuarios").doc(props.idToken.email).collection("Cesta").doc(nombre).set({
                id: id,
                nombre: nombre,
                cantidad: cantidad,
                precio: precio,
                rutaImagen: rutaImagen
            });
            actualizarPedido();
        } else {
            alert("Tienes que estar logueado.")
        }
    }

    const eliminarProductoCesta = () => {
        if (props.idToken.idToken != undefined) {
            cantidad = buttonClicked - 1;
            setButtonClicked(cantidad);
            if (buttonClicked > 1) {
                db.collection("Usuarios").doc(props.idToken.email).collection("Cesta").doc(nombre).set({
                    id: id,
                    nombre: nombre,
                    cantidad: cantidad,
                    precio: precio,
                    rutaImagen: rutaImagen
                });
            } else {
                db.collection("Usuarios").doc(props.idToken.email).collection("Cesta").doc(nombre).delete();
            }
            actualizarPedido();
        } else {
            alert("Tienes que estar logueado.")
        }
    }

    return (
        <div className="card col-sm-4 col-6" id="div_producto">
            <img src={rutaImagen} id='imagen_producto' className="card-img-top"></img>
            <div className="card-body row">
                <h5 className="card-title col">{nombre}</h5>
                <h5 className="card-text col">{precio}€</h5>
                <div className="botones_producto" id={id}>
                    <Button className="btn btn-primary btn_producto"  onClick={anadirProductoCesta}>+</Button>
                    <Button className="btn btn-danger btn_producto" onClick={eliminarProductoCesta}>-</Button>
                </div>

            </div>
        </div>
    );
}

export default Producto;