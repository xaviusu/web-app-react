import Button from "react-bootstrap/esm/Button";
import Modal from "react-bootstrap/esm/Modal";

import { db } from "../firebase";
import { useEffect, useState } from "react";
import "./TramitarPedido.css"
import { Link } from "react-router-dom";

function TramitarPedido(props) {
  const [productos, setProductos] = useState([]);
  const [precioTotal, setPrecioTotal] = useState(0);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  let arrayProductos = [];

  useEffect(() => {
    db.collection('Usuarios').doc(props.loginData.email).collection("Cesta")
      .get()
      .then(querySnapshot => {
        let precioPedido = 0;

        querySnapshot.forEach(doc => {
          var elemento = doc.data();
          arrayProductos.push({
            id: elemento.id,
            nombre: elemento.nombre,
            precio: elemento.precio,
            cantidad: elemento.cantidad
          });
          precioPedido += parseFloat(elemento.precio * elemento.cantidad, 2);
        });

        setPrecioTotal(precioPedido);
        setProductos(arrayProductos);
      }).catch(error => {
        console.log('Se ha producido un error ' + error);
      });
  }, []);

  const tramitar = function () {
    let newDate = new Date().getTime();
    let fecha = new Date().getDay() + "/" +
      (new Date().getDate() - 1) + "/" +
      new Date().getFullYear();
    let horas = new Date().getHours() < 10 ? "0" + new Date().getHours() : new Date().getHours();
    let minutos = new Date().getMinutes() < 10 ? "0" + new Date().getMinutes() : new Date().getMinutes();
    let hora = horas + ":" + minutos;

    db.collection("Usuarios").doc(props.loginData.email)
      .collection("Cesta").get()
      .then(querySnapshot => {
        db.collection("Usuarios").doc(props.loginData.email).collection("Pedidos").doc(newDate.toString()).set({
          fechaPedido: fecha,
          horaPedido: hora,
          precioTotal: precioTotal
        });
        querySnapshot.forEach(doc => {
          var elemento = doc.data();
          db.collection("Usuarios").doc(props.loginData.email).collection("Pedidos").doc(newDate.toString()).collection("Detalle pedido").doc(elemento.nombre).set({
            id: elemento.id,
            nombre: elemento.nombre,
            cantidad: elemento.cantidad,
            precio: elemento.precio
          })
        });
        productos.forEach(element => {
          let nombre = element.nombre;
          db.collection("Usuarios").doc(props.loginData.email)
            .collection("Cesta").doc(nombre).delete();
        });
        handleClose();
      });
  }
  
  return (
    <div>
      <div>
        <div id="card_tramitar" className="card">
          <div className="card-body">
            <ul className="list-group list-group-flush">
              {productos.map((elemento) => (
                <li className="list-group-item">
                  {elemento.nombre} x {elemento.cantidad} = {elemento.precio}
                </li>
              ))}
              <li id="liPagar" className="list-group-item container">
                <Button onClick={handleShow} id="botonPagar" className="col-9">Pagar - {precioTotal}€</Button>
              </li>
            </ul>
          </div>
        </div>
        
      </div>
      <Modal.Dialog hidden={!show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Confirmar la compra</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <p>¿Desea continuar a pagar?</p>
          </Modal.Body>

          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>No</Button>
            <Button variant="primary" onClick={tramitar}><Link style={{ textDecoration: 'none', color: "white" }} to="/">Sí</Link></Button>
          </Modal.Footer>
        </Modal.Dialog>
    </div>
    
  )
}

export default TramitarPedido;