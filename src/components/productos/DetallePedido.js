import "./DetallePedido.css"

function DetallePedido(props) {
    const nombre = props.nombre;
    const cantidad = props.cantidad;
    const precio = props.precio;
    
    return (
        <div>
            <div id="div_detalle_pedido">{cantidad} X {nombre} = {precio}</div>
        </div>
    )
}

export default DetallePedido;