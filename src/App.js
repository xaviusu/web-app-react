import Login from './components/login/Login';
import Registro from './components/login/Registro';
import Header from './components/UI/Header';
import Principal from './components/Principal';
import TramitarPedido from './components/productos/TramitarPedido';
import './App.css';
import { useState } from 'react';
import { Routes, Route } from 'react-router-dom';
import Pedidos from './components/productos/Pedidos';

function App() {
  const [login, setLogin] = useState(false);
  const [loginData, setLoginData] = useState({});

  const actualizaLogin = (valor, datos) => {
    setLogin(valor);
    setLoginData(datos);
  }

  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<Principal loginData={loginData} />} />
        <Route path="/login" element={<Login actualizaLogin={actualizaLogin} />} />
        <Route path="/registro" element={<Registro actualizaLogin={actualizaLogin} />} />
        <Route path="/tramitar" element={<TramitarPedido loginData={loginData} />} />
        <Route path="/pedidos" element={<Pedidos loginData={loginData} />} />
      </Routes>
    </>
  );
}

export default App;
